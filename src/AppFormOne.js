import React from 'react';

class AppFormOne extends React.Component {
  render() {
    return (
      <div className="app-box">
        <h3 className="app-box--title">Your Income & Spend</h3>
        <form className="app-income-form">
          <h4 className="app-box--subtitle">Annual income</h4>
          <div className="app-form-row">
            <div className="app-form-group app-col">
              <label htmlFor="inputAnnualSalary">Annual salary:</label>
              <input type="number" className="app-form-control" min="0" max="45300" name="AnnualSalary" id="inputAnnualSalary" placeholder="&pound; 45,300"/>
            </div>
            <div className="app-form-group app-col">
              <label htmlFor="inputFromAgeAnnualSalary">From age:</label>
              <input type="number" className="app-form-control" disabled="disabled" name="FromAgeAnnualSalary" id="inputFromAgeAnnualSalary" placeholder="30"/>
            </div>
            <div className="app-form-group app-col">
              <label htmlFor="inputToAgeAnnualSalary">To age:</label>
              <input type="number" className="app-form-control" disabled="disabled" name="ToAgeAnnualSalary" id="inputToAgeAnnualSalary" placeholder="67"/>
            </div>
          </div>
          <h4 className="app-box--subtitle">Monthly spending</h4>
          <div className="app-form-row">
            <div className="app-form-group app-col">
              <label htmlFor="inputMortgage">Mortgage:</label>
              <input type="number" className="app-form-control" min="0" max="1199" name="Mortgage" id="inputMortgage" placeholder="&pound; 1199"/>
            </div>
            <div className="app-form-group app-col">
              <label htmlFor="inputFromAgeMortgage">From age:</label>
              <input type="number" className="app-form-control" disabled="disabled" name="FromAgeMortgage" id="inputFromAgeMortgage" placeholder="30"/>
            </div>
            <div className="app-form-group app-col">
              <label htmlFor="inputToAgeMortgage">To age:</label>
              <input type="number" className="app-form-control" disabled="disabled" name="ToAgeMortgage" id="inputToAgeMortgage" placeholder="65"/>
            </div>
          </div>
          <div className="app-form-row">
            <div className="app-form-group app-col">
              <label htmlFor="inputBills">Bills:</label>
              <input type="number" className="app-form-control" min="0" max="1199" name="Bills" id="inputBills" placeholder="&pound; 1199"/>
            </div>
            <div className="app-form-group app-col">
              <label htmlFor="inputFromAgeBills">From age:</label>
              <input type="number" className="app-form-control" disabled="disabled" name="FromAgeBills" id="inputFromAgeBills" placeholder="30"/>
            </div>
            <div className="app-form-group app-col">
              <label htmlFor="inputToAgeBills">To age:</label>
              <input type="number" className="app-form-control" disabled="disabled" name="ToAgeBills" id="inputToAgeBills" placeholder="65"/>
            </div>
          </div>
          <div className="app-form-row">
            <div className="app-form-group app-col">
              <label htmlFor="inputGeneralSpending">General spending:</label>
              <input type="number" className="app-form-control" min="0" max="1199" name="GeneralSpending" id="inputGeneralSpending" placeholder="&pound; 1199"/>
            </div>
            <div className="app-form-group app-col">
              <label htmlFor="inputFromAgeGeneralSpending">From age:</label>
              <input type="number" className="app-form-control" disabled="disabled" name="FromAgeGeneralSpending" id="inputFromAgeGeneralSpending" placeholder="30"/>
            </div>
            <div className="app-form-group app-col">
              <label htmlFor="inputToAgeGeneralSpending">To age:</label>
              <input type="number" className="app-form-control" disabled="disabled" name="ToAgeGeneralSpending" id="inputToAgeGeneralSpending" placeholder="65"/>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default AppFormOne;
