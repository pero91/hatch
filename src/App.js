import React from 'react';
import AppHeader from './AppHeader';
import AppFormOne from './AppFormOne';
import AppFormTwo from './AppFormTwo';
import './App.css';
import './Data.json';

class App extends React.Component {
  render() {
    return (
      <div className="app">
        <AppHeader></AppHeader>
        <div className="app-main">
          <div className="app-container">
            <div className="app-row">
              <div className="app-col">
                <AppFormOne></AppFormOne>
              </div>
              <div className="app-col">
                <AppFormTwo></AppFormTwo>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
