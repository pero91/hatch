import React from 'react';
import Tooltip from 'rc-tooltip';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';

const Handle = Slider.Handle;

const handle = (props) => {
  const { value, dragging, index, ...restProps } = props;
  return (
    <Tooltip
      prefixCls="rc-slider-tooltip"
      overlay={value}
      visible={dragging}
      placement="top"
      key={index}
    >
      <Handle value={value} {...restProps} />
    </Tooltip>
  );
};

class AppFormTwo extends React.Component {
  render() {
    return (
      <div className="app-box">
        <h3 className="app-box--title">Spend less</h3>
        <form className="app-income-form">
          <p className="app-box--paragraph">Try reducing your monthly spending to see how your forecast could improve!</p>
          <div className="app-form-group">
            <label htmlFor="sliderMortgage">Mortgage:</label>
            <Slider min={0} max={1199} defaultValue={743} handle={handle} />
          </div>
          <div className="app-form-group">
            <label htmlFor="sliderBills">Bills:</label>
            <Slider min={0} max={1199} defaultValue={198} handle={handle} />
          </div>
          <div className="app-form-group">
            <label htmlFor="sliderGeneralSpending">General spending:</label>
            <Slider min={0} max={1199} defaultValue={600} handle={handle} />
          </div>
          <div className="app-form-group app-form-group--results">
            <p className="app-box--paragraph__results">This means you are saving <strong>&pound; 230</strong> per month</p>
          </div>
          <div className="app-form-group app-form-group--action">
            <a href="https://www.google.com/" className="app-btn app-btn--ghost" target="_blank" rel="noopener noreferrer">Find ways to save</a>
          </div>
          <div className="app-form-group app-form-group--actions">
            <span>Was this helpful?</span>
            <a href="" className="app-icon app-icon--up" title="Yes"><span className="sr-only">Yes</span></a>
            <a href="" className="app-icon app-icon--down" title="No"><span className="sr-only">No</span></a>
          </div>
        </form>
      </div>
    );
  }
}

export default AppFormTwo;
